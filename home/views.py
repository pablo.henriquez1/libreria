from django.shortcuts import render


# Create your views here.
from django.views.generic import ListView
from .models import *

def index(request):
    return render(request,'home.html')

class listaAutores(ListView):
    template_name = 'autores.html'
    model=Autor
    context_object_name = 'autores'

