from django.urls import path

from home import views
from home.views import *

urlpatterns = [
    path('', index,name='home'),
    path('autores',views.listaAutores.as_view(),name='listaAutores'),
]