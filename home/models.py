from django.db import models

# Create your models here.
class Persona(models.Model):
    nombre=models.CharField('Nombre',max_length=150,null=True)
    apellido= models.CharField('Apellido', max_length=150, null=True)
    nac=[('1',"Chilena"),('2',"Venezolana"),('3',"Peruana"),('4',"Ecuatoriana"),('5',"Colombiana"),('6',"otra")]
    nacionalidad=models.CharField("Nacionalidad",max_length=20,choices=nac)
    class Meta:
        abstract=True

class Autor(Persona):
    pass

class Cliente(Persona):
    rut= models.CharField("Rut",max_length=15,null=False,blank=False,primary_key=True)
    direccion=models.TextField("Dirección",max_length=200,blank=True)
    telefono=models.CharField("Teléfono",max_length=10,blank=False,null=False)
    def __str__(self):
        return self.rut, self.nombre

class Libros(models.Model):
    isbn=models.CharField("ISBN",max_length=7,null=False,primary_key=True)
    titulo=models.CharField("Titulo",max_length=100,null=True)
    autor=models.ForeignKey(Autor,on_delete=models.CASCADE)
    def __str__(self):
        return self.titulo,self.autor